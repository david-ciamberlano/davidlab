+++
title= "Red Hat Open Source Day 2019"
date= 2020-01-17T18:12:48+01:00
tags = ["alfresco","eventi"]
categories = ["events","my activities"]
featureimage = ""
toc = "true"
draft = "false"
+++

{{< figure src="/img/red-hat-osd-2019/Alfresco-open-source-day.jpg" caption="" >}}

> Il Red Hat Open Source Day è il luogo e il modo in cui le idee migliori diventano realtà, dove  imparare, fare networking e capire l’essenza dell’open source. È l’unione di ispirazione e azione, per lavorare su ciò che hai, verso ciò che vuoi, puntando ai tuoi obiettivi di business futuri [dalla pagina ufficiale del Red Hat Open Source Day 2019]

<!--more-->

Alfresco, dopo qualche anno di "pausa", nel 2019 è finalmente di nuovo presente a quella che, senza mezzi termini, è la manifestazione dedicata all’open source più importante in Italia. Questo grazie agli sforzi del Team Alfresco Italia e al supporto del Marketing e di alcuni colleghi d’oltralpe.

Due tappe: Roma, il 20 novembre al Palazzo dei Congressi; Milano, il 3 dicembre as MiCo. L’ottima organizzazione ci ha messo a disposizione uno stand che abbiamo allestito con i nostri gadget , depliant e loghi (avevamo anche due bellissimi banner alti 2 metri che però ci hanno fatto prontamente rimuovere in quanto non permessi…).

## Lo stand
Presso lo stand abbiamo accolto numerosi "curiosi" interessati ad avere maggiori informazioni sui nostri prodotti o a capire meglio qual era la nostra proposta… Abbiamo chiacchierato sul futuro, dispensato informazioni, consigli e naturalmente fatto business (eravamo lì a posta!). I contatti non sono mancati, vedremo nei prossimi mesi se si tramuteranno in buone opportunity.
(Ci sono stati anche molti "gadget-hunters" come li abbiamo rinominati che si avvicinato attratti dai numerosi gadget in bella vista sul desk… ma penso facciano parte del folklore di qualsiasi manifestazione pubblica).
L’evento è stata anche un’ottima occasione per avvicinare gli stand di altre tecnologie interessanti (anche noi abbiamo bisogno di informarci dopotutto!), per fare networking con altri Sales/presales e anche per rincotrare vecchie conoscenze… il mondo dell’IT è piccolo dopotutto.

![Lo stand (in fase di allestimento) di Alfresco](/img/red-hat-osd-2019/RHOSD-desk.png)



## L’evento
Il format dell’evento è stato lo stesso nelle due sessioni di Roma e Milano: welcome coffee iniziale seguito da una suggestiva performance di artisti e quindi i Keynotes della mattina tenuti dai grandi player. Nella seconda parte della giornata, dopo il buffet, erano invece concentrate le sessioni parallele dei partner… cui ho partecipato anche io con una mia presentazione (in veste ufficiale Alfresco).

## I Talk
Nel pomeriggio, sia a Roma che a Milano, ho tenuto un Talk su alcuni aspetti della Digital Transformation e su come un software di Enterprise Content Management (Alfresco, ovviamente) può essere di supporto per le aziende che decidono di affrontare il cambiamento.

(cito dalle slides)
> Molti aspetti della Digital Transformation sono correlati in qualche modo (a volte anche strettamente) alla gestione dei documenti e dei flussi di informazioni.
>
> Ci sono strumenti che possono aiutare a distribuire più efficacemente **informazioni** e **idee**.
>
> L’**information flow** è la chiave! L’integrazione di Contenuti e Processi Permette di presentare: l’informazione corretta, alla persona corretta, nel momento corretto.
> Ma le aziende devono anche integrarsi con un complesso sistema giuridico e non è possibile ignorarlo. Il **Governance Service** permette di conformarsi agli obblighi legali più agevolmente.

Il pdf delle mie slide è pubblicamente disponibile sul sito dell’evento: [RDOSD-2019-Ciamberlano-slides](https://images.engage.redhat.com/Web/RedHat/%7Bba8eff94-0132-4793-a9f3-33f2bf6239e1%7D_3._Alfresco.pdf)


![Il mio talk sul alcuni aspetti della Digital Transformation](/img/red-hat-osd-2019/RHOSD-speech.png)




