+++
title = "Alfresco Devcon 2018"
date = "2018-02-08T23:00:00+01:00"
tags = ["alfresco","eventi"]
categories = ["events","my activities"]
toc = "true"
+++

Ho partecipato all’Alfresco Devcon 2018… ed &egrave; stato fantastico!

Ho incontrato persone assolutamente speciali, ascoltato conferenze divertenti e interessanti, tenuto ben due lightning talk, ricevuto un premio per i miei contributi alla community… e pi&ugrave; di tutto mi sono divertito un mondo!

Potreste anche fermarvi qui… ho detto tutto l’essenziale.

Se invece volete scoprire qualche dettaglio in pi&ugrave; su quello che &egrave; successo a Lisbona nei due giorni a met&agrave; gennaio, condito di qualche aneddoto (forse) divertente, continuate a leggere…
<!--more-->

## Il DevCon 2018

Dopo qualche anno di letargo, finalmente quest’anno &egrave; stato organizzato un nuovo DevCon, una conferenza di respiro internazionale (e lo era davvero… c’erano persone dal Sudafrica, dall’Australia, dal Brasile e persino dal Giappone!) interamente dedicata ad Alfresco.

La cosa che pi&ugrave; mi &egrave; piaciuta &egrave; stata che non c’erano solo(!) membri illustri della community ma soprattutto gli sviluppatori delle maggiori tecnologie che compongono Alfresco. Poter ascoltare talk su ADF o APS tenuti direttamente da chi quei componenti li sta realizzando &egrave; stata una esperienza impagabile.

La location era suggestiva: gli ultimi due piani del Museo du Oriente con un enorme terrazzo che affacciava proprio sul ponte pi&ugrave; famoso di Lisbona. 

Il cibo (eh s&igrave; da italiano non posso non esprimere un giudizio anche su questo aspetto) era…  come posso dire… passabile.

Il tutto &egrave; stato ben organizzato. Nonostante i molti partecipanti, non si percepiva la sensazione di eccessivo affollamento. Gli spostamenti erano agevoli e i talk sono filati tutti liscissimi (eccetto per qualche piccolo problema tecnico di tanto in tanto ma che non ha almeno a mio parere impattato sulla qualit&agrave; degli interventi).

![Il mio premio per i contributi nella community](/img/devcon2018/devcon2018-gadget.jpg)

## I miei lightning talk

E’ stata probabilmente l’incoscienza a spingermi a proporre ben due talk (erano lightning talk…  perch&eacute; sarei stato un pazzo a scegliere da subito le sessioni lunghe!) in inglese di fronte ad un pubblico internazionale.

Fatto sta che, in prima battuta, ne &egrave; stato scelto uno… e io ero gi&agrave; contentissimo cos&igrave; perch&eacute; a quanto pare la selezione &egrave; stata severa e c’&egrave; stato  un altissimo numero di proposte respinte.
Poi dopo un paio di settimane mi &egrave; stato comunicato che era stato selezionata anche la mia **seconda submission**...

Notizia splendida... ma che ha portato il mio livello di tensione, gi&agrave; elevato, a schizzare verso l’alto (perch&eacute; si sa che il livello di tensione sale con il quadrato del numero di talk) .
Ora, per chi non conoscesse i lightning talk (e io non li conoscevo prima di questo evento) posso dire che sono degli interventi veloci della durata di 5 minuti.  Bisogna condensare tutto il 20 slide che verranno proiettate in auto-avanzamento ogni 15 secondi.

Inutile dire che non c’&egrave; tempo per pensare… e si riescono ad enunciare al massimo una o due frasi per ogni slide.

In queste occasioni, dove c’&egrave; un alto grado di tensione, bisogna essere ben preparati per non fare brutte figure.

Ho finito per imparare entrambi i talk a memoria a forza di ripeterli… (e nonostante questo l’emozione del momento mi ha spinto a "leggere le slide" in pi&ugrave; di una occasione, cosa che non si dovrebbe fare… ma va bene cos&igrave;).

![Io durante uno dei miei lightning talk](/img/devcon2018/devcon2018-lightning-talk.jpg)
![Io durante uno dei miei lightning talk](/img/devcon2018/devcon2018-david-on-stage.jpg)

Le slide di entrambi i miei talk sono su slide share:

* [David & goliath](https://www.slideshare.net/DavidCiamberlano/david-goliath-86763647) 
* [You probably didn’t know that](https://www.slideshare.net/DavidCiamberlano/you-probably-didntknowthat)

## Il premio per la mia attivit&agrave; nella community

Eh s&igrave;... ho ricevuto anche un (inaspettatissimo) premio per la mia attivit&agrave; nella community.

Durante la pausa pranzo dell’ultimo giorno &egrave; stato improvvisato (in gran segreto) un banchetto pieno di strani gadget. Poi da un microfono spuntato quasi dal nulla sono stati chiamati 20 membri della comunit&agrave; che si erano distinti per le loro attivit&agrave; nella community. E tra quelli c’ero anche io!

Naturalmente &egrave; stato un grandissimo onore per me… ed ero a dir poco stracontento (come testimionia una delle foto qui sotto).

Tornato a casa ho per&ograve; quel premio ha cominciato a far sentire il suo peso…  E’ stato un premio per le cose fatte in passato… ma &egrave; un po’ come se dovessi meritarmelo anche nel futuro.  Ve lo devo dire? Sfida accettata… ho in mente delle belle cose per il 2018.

Siete curiosi di sapere cos’era il premio: un cubo di rubik edizione speciale.

![Il mio premio per i contributi nella community; Io e Francesco Corti, product evangelist di Alfresco](/img/devcon2018/devcon2018-vip-award.png)
