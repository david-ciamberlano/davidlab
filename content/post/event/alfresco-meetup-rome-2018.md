+++
title = "Alfresco Meetup Rome 2018"
date = 2018-07-20T23:45:44+02:00
tags = ["alfresco","eventi"]
categories = ["events","my activities"]
toc = "true"
+++

{{< figure src="/img/alfresco-meetup-2018/logo_alfresco_meetup_roma.png" caption="" >}}

## La definizione di "successo"

Partiamo dalla definizione di "successo"... perché una domanda che mi sono posto molte volte mentre cercavo di organizzare l'Alfresco Meetup era: *in base a quali parametri potrò dire se l'evento sarà andato bene?*

Anche se sembra una pura speculazione narcisistica, la risposta a questa domanda era molto importante, perchè doveva indicarmi la direzione verso la quale indirizzare le energie.

E come per tutte le cose importanti, una risposta "giusta" ovviamente non c'era...

<!--more-->

La migliore che sono riuscito a trovare è stata:

*"sarà un successo se i partecipanti avranno modo di divertertirsi, conoscere nuova gente e impare qualcosa di utile"*

Ora, a qualche giorno di distanza (e in base ai feedback raccolti) posso dire che **abbiamo** raggiunto l'obiettivo!

L'uso del plurale non è un caso, perchè un evento del genere non lo può organizzare un solo individuo. Nel mio caso ho avuto l'aiuto (morale e materiale) di alcune fondamentali persone che ho già ringraziato durante l'evento ma che vorrei (ri)ringraziare anche qui.

## A note of thanks...

Poichè non c'è un ordine di importanza, procederò (come si fa nei film) in ordine di apparizione, ovvero nell'ordine in cui sono stati coinvolti nell'avventura.

Innanzitutto sono grato a  **Logica Informatica** che ci ha dato (in modo completamente gratuito) i locali... e in particolare **Aldo Pergjerji** che con la sua grande esperienza ha saputo elargire ottimi consigli su come strutturare al meglio l'evento.

Poi un grande thank you va ad **Alfresco** e –visto che dietro le aziende ci sono sempre delle persone– in particolare a **Francesco Corti** e **Kristen Gastaldo**, per il supporto morale e materiale... e per i gadget andati a Ruba!

Grazie naturalmente anche **Piergiorgio Lucidi**, che non rifiuta mai di dare il suo contributo quando si tratta di community (anche se di impegni ufficiali ne avrebbe in quantità)!

Infine la mia riconoscenza va a **tutti** quelli che hanno partecipato, perchè senza di loro il meetup non avrebbe avuto ragione di esistere.

## Gli interventi

Erano programmati tre talk, da circa 20 minuti ciascuno. Un'ora in totale... un tempo giusto per non "provare" troppo i circa 15 partecipanti, considerato che l'evento si svolgeva in coda ad una giornata lavorativa. Ecco di cosa si è parlato:

### Francesco Corti: "Fammi vedere come si fa con ADF (Angular)"

Francesco Corti –Product Evangelist di Alfresco e attivissimo membro della community– ha fatto una scommessa con i partecipanti: riuscire a realizzare una app ADF(\*) in 3 minuti... da zero... live!

Converrete che era una sfida non da poco! Inutile dire che Francesco non solo è riuscito a fare quanto promesso ma ha anche saputo rendere la sessione divertente, con il suo stile brillante ed informale!

> (\*): per chi viene dalla luna (o più semplicemente non conosce Alfresco), [ADF](https://community.alfresco.com/community/application-development-framework/pages/get-started) è un framework open source basato su Angular che permette di creare  applicazioni web client che interagiscono con Alfresco.

{{< figure src="/img/alfresco-meetup-2018/talk-fcorti.jpg" caption="Francesco Corti durante il suo Talk" >}}

### Piergiorgio Lucidi: "Smart Content Migration using Apache ManifoldCF"

Piergiorgio Lucidi –Chief Technology Evangelist, membro dell'Apache Software Foundation, Mentor e Committer in vari progetti open-source, chitarrista Jazz e, nel tempo libero, uno dei massimi esperti di Alfresco in Italia– ha presentato lo stato dell'arte di quello che potrebbe diventare il tool di riferimento per la migrazione dei contenuti, non solo in Alfresco: [Apache ManifoldCF](https://manifoldcf.apache.org) (di cui è PMC Member).

Le slides del suo talk le trovate su Slide Share: [Smart Content Migration using Apache ManifoldCF](https://www.slideshare.net/PiergiorgioLucidi/smart-content-migration-using-apache-manifoldcf)

![Piergiorgio Lucidi parla di community e di content migration](/img/alfresco-meetup-2018/talk-pj-lucidi.jpg)

### David Ciamberlano: "ACS deploy con Docker"

Sì, l'ultimo intervento è stato mio (scusate se non mi presento).
Ho parlato della nuova modalità di deploy di Alfresco, che diverrà standard tra non molto: **Docker & Kubernetes**.

Visto che l'argomento è davvero nuovissimo e che molti non hanno familiarità con docker, ho provato a fare una breve introduzione sul mondo dei container e a spiegare perchè in Alfresco si sono decisi ad abbracciare questa nuova modalità di installazione. Subito dopo ho mostrato una demo live di come si può costruire un sistema Alfresco in pochi secondi usando *docker-compose*.

Anche le mie slides le trovate su Slide Share: [Alfresco meetup Roma - docker](https://www.slideshare.net/DavidCiamberlano/alfresco-meetup-roma-docker)

![Il mio alter-ego che presenta il mio talk](/img/alfresco-meetup-2018/talk-david.jpg)

## Networking (finalmente)!

Parte fondamentale di un meetup degno di tale nome è il Networking... che poi, da buoni italiani, si traduce in: fare quattro chiacchiere in tranquillità con un bicchiere di vino e del buon cibo, ognuno parlando della propria esperienza.

Logica Informatica si è rivelata all'altezza anche in questo caso, mettendoci a disposizione una tranquilla e fresca veranda dove mangiare e muoversi agevolmente tra i vari gruppetti.

E' stata una parte piacevole del meetup (guardate le foto!) ed un'ottima occasione per scambiare due chiacchiere con vecchi colleghi e nuove conoscenze.

![Networking](/img/alfresco-meetup-2018/the-networking.jpg)
