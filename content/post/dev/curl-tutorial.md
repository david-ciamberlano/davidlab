+++
title = "Curl Tutorial"
date = 2019-11-10T15:41:08+01:00
type = "post"
draft = "false"
toc = "true"
tags = ["dev","curl"]
categories = ["Development"]
featureimage = ""
menu = ""
+++

Curl è uno strumento a linea di comando estremamente completo (e complesso) per eseguire http requests.

Questo post non pretende di essere esaustivo ma ha l’unico scopo di mettervi in grado di utilizzare alcune utili funzioni di curl nel più breve tempo possibile.

<!--more-->

Come prerequisito bisogna avere una conoscenza di base del protocollo http.

Procurarsi curl è molto semplice. Su ambienti linux lo avete quasi certamente installato di default. Su windows dovete invece scaricare e installare un pacchetto binario. Per comodità potete aggiungere il path della vostra installazione alla variabile d’ambiente PATH in modo da poter accedere al tool da una qualsiasi posizione.

## Le basi di curl

Per verificare la corretta installazione di curl potete aprire una shell e digitare:
```
curl --version
restituisce un messaggio del genere:
curl 7.30.0 (i386-pc-win32) libcurl/7.30.0 zlib/1.2.7
Protocols: dict file ftp gopher http imap ldap pop3 rtsp smtp telnet tftp
Features: AsynchDNS IPv6 Largefile libz
```
Provate poi a digitare:
```
curl --help
```
Non fatevi spaventare dalla quantità di opzioni che vi vengono proposte… curl è un tool molto potente e consente di fare una miriade di operazioni. Questo non vuol dire che le dobbiate conoscere tutte (per fortuna!).

La guida che viene stampata sullo schermo costituisce un utile aiuto per la vostra memoria per ricordare come e quali parametri utilizzare per un certo compito. Come noterete quasi tutti i comandi hanno due possibili espressioni equivalenti, una lunga (in genere inizia con il doppio trattino --) e una abbreviata costituita da un trattino seguito da un’unica lettera.

I due comandi seguenti producono lo stesso risultato

```
curl --help
curl -h
```

Nel seguito utilizzerò prevalentemente la forma abbreviata.


### Recuperare una pagina (o invocare un servizio) web

```
curl "http://www.example.com"
```

Le virgolette in questo caso possono essere omesse.
 
Per recuperare una pagina web e farne il dump dell’output su file

```
curl -o index.html "http://www.example.com"
```

oppure

```
curl "http://www.example.com" > index.html
```

L'output su schermo che si ottiene e simile a questo:

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current 
                                 Dload  Upload   Total   Spent    Left  Speed   
100  1270  100  1270    0     0   2466      0 --:--:-- --:--:-- --:--:--  2540 
```

(nell’esempio il dump è eseguito su un file di nome index.html, nella directory corrente)

Insieme ai dati veri e propri, potrebbe essere interessante visualizzare anche qualche informazione in più come ad esempio gli header del protocollo http.

Con l’opzione **-i** possiamo ottenere anche gli header inviati scambiati server (per controllare l’esito delle nostre richieste).
```
curl -i "http://www.example.com"
```
Con l’opzione **-I** otteniamo invece solo gli header senza i dati.

Invece con lo switch **-v** (verbose) possiamo ottenere messaggi più dettagliati sulle operazioni compiute (tra cui i soliti header).

### Invocare un metodo http (GET, POST, DELETE, PUT)

Per comporre delle chiamate utilizzando specifici metodi http si usa il parametro -X  seguito dal nome del metodo

NOTA: allo scopo di provare i vari metodi http e di avere un responso diretto sulla loro funzionamento utilizzerò il servizio gratuito httpbin.org che non è altro che un echo-server per le chiamate http.
```
curl -X GET "http://www.example.com/"
curl -X DELETE "http://httpbin.org/delete/id/123"
```
con lo switch -d possiamo passare dei parametri in caso, ad esempio di chiamate POST:

```
curl -X POST "http://httpbin.org/post" -d "username=myun&password=mypw"
```

Otterremo come risposta un json che in un certo punto riporta i parametri da noi inviati
```
[...]
    "username": "myun",
    "password": "mypw"
[...]
```
se abbiamo invece bisogno d'inviare un file binario possiamo utilizzare l’opzione –data-binary e il nome del file da inviare preceduto da @
```
curl -X PUT "http://httpbin.org/put" --data-binary @data-file-name.txt
```
è anche possibile codificare i dati in modo che siano adatti ad essere inviati sul web
```
curl -X POST "http://httpbin.org/post" --data-urlencode "stringa da codificare"
```
Se abbiamo bisogno d'includere header extra alla nostra richieste (ad esempio il content-type) possiamo utilizzare il parametro -H
```
curl -X POST "http://httpbin.org/post" -H "Content-Type: application/json" -d "{'data':'hello!'}"
```
per fare l’upload di un file possiamo usare in parametro -F che in pratica simula l’invio di un form (multipart-post-data)
```
curl "http://httpbin.org/post" -F "uploaded_file=@nomefilelocale.ext" -F "name=test-upload" 
```
È possibile utilizzare -F più volte per inviare dati aggiunti oppure è possibile utilizzare lo switch -T (eventualmente con nomi di file multipli racchiusi tra parentesi graffe… attenzione a non inserire spazi prima e dopo la virgola!)
```
curl -T "{file1.xxx,file2.xxx}" "http://httpbin.org/put"
```
## Altre funzioni utili

Per eseguire l’autenticazione (basic authentication) inviando username e password si può usare -u
```
curl -u username:password http://httpbin.org/get
```
se viene specificato solo lo username, curl si preoccuperà di chiedervi anche la password

### Test di una connessione FTP:
```
curl -v -T <nome-file-da-caricare> ftp://user:pass@localhost:2121
```
