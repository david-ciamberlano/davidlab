+++
title = "Presales 101.1 - Strategia e Tattica"
date = 2022-01-15T10:10:00+00:00
type = "post"
draft = "false"
toc = "true"
tags = ["presales"]
categories = ["presales"]
menu = ""
+++

Presales è solo uno dei tanti nomi che vengono usati per indicare una particolare figura professionale. Ma chi è davvero un presales? In questo post cercheremo di definire un po' meglio questo ruolo.

<!--more-->

## "Definizione" di presales

Presales è solo uno dei tanti nomi che vengono usati per indicare una particolare figura professionale. Ve ne cito solo alcuni: Solutions Engineer, Technical Sales, Solution Designer, o ancora: Sales Engineer, Value Consultant (PS: io, da qui in avanti, userò ora l’uno ora l’altro come se fossero sinonimi, anche se non sempre lo sono).

Ciascuno di questi nomi tenta di cogliere certi aspetti del ruolo... finendo però inevitabilmente per trascurarne altri. Questo è normale che accada quando si cerca a riassumere un concetto molto ampio in poche parole: quelle parole quasi mai sono sufficientemente espressive per caratterizzarlo correttamente.

Se cerchiamo di capire cosa fa un “presales” semplicemente analizzandone i molteplici nomi, non otteniamo molto.

Tuttavia, queste definizioni qualche indicazione la danno: **pre-sales**, ad esempio, indica qualcosa che avviene prima della finalizzazione di un contratto; **Technical Sales** ci suggerisce un ruolo tecnico ma anche coinvolto nelle vendite; infine **Solutions Engineer** e **Value consultant** suggeriscono la capacità di trovare soluzioni e di generare valore.

Partiamo dunque da questi suggerimenti per costruire una descrizione che sia quanto più possibile comprensibile.

## Il processo di vendita

Per poter andare avanti, è necessario prima fare una breve digressione.
Da quanto detto, è emerso che il presales è in qualche modo coinvolto nelle vendite. Ma cos’è una vendita?

Nell’immaginario comune, viene raffigurata più o meno così (parliamo di vendite B2B non vendite al dettaglio ovviamente):

Un potenziale cliente contatta la nostra azienda e tipicamente viene intercettato da un commerciale, che organizza con lui alcuni incontri per illustrare i prodotti e le soluzioni che ha nel portafoglio.
Segue una fase di contrattazione, si tira un po’ sul prezzo e sulle funzionalità da includere o escludere nell’accordo e -finalmente- si giunge alla firma del contratto, sancita con una vigorosa stretta di mano.

Ecco, se tutte le vendite fossero come questa, il presales semplicemente… sarebbe inutile e non avrebbe ragione di esistere!

Fortunatamente (o sfortunatamente) le cose non sono sempre così semplici.

Soprattutto quando il nostro portafoglio è composto molteplici prodotti non banali, il potenziale cliente di grandi dimensioni e (di conseguenza) i budget in gioco sono elevati, la vendita diventa qualcosa di molto più complesso.

Si parla infatti di processo di vendita, che prevede numerose fasi, ciascuna delle quali può essere declinata in varie forme a seconda di variabili come il tipo di azienda, il settore, l’ambito del progetto, …

Sui manuali queste fasi sono descritte in maniera dettagliata e ci sono anche delle metodologie che insegnano come affrontare al meglio ciascuno step.

In questa sede, non è necessario andare nel dettaglio. È sufficiente tenere a mente che un processo di vendita inizia con un primo contatto, cui seguono molte fasi intermedie e si conclude -nel migliore dei casi- nella firma di un contratto.

Non a caso ho scritto “nel migliore dei casi”, perché purtroppo questo processo può interrompersi in qualsiasi momento, per i motivi più disparati. E se si interrompe, noi non otteniamo nulla.

Questo è spiacevole perché la vendita è spesso molto dispendiosa sia per energie che per tempo impiegato. Tanto per darvi un’idea, dal primo contatto con il cliente alla firma finale possono volerci mesi di lavoro, nei casi peggiori anche anni. Quindi potete immaginare che non è affatto confortante vedere tutti gli sforzi profusi fino a quel momento svanire nel nulla.

Ma c’è di peggio: solitamente (questo accade sempre per i Sales e molto spesso per i presales) si è pagati con una quota variabile legata alle vendite andate a buon fine nell’anno fiscale. Perdere un contratto, quindi, non è solo demoralizzate ma può comportare un danno economico (cosa che non fa piacere a nessuno).

Seguire le Best Practices nelle vendite può minimizzare (purtroppo mai annullare) la probabilità che questo accada.

## Il ruolo del presales

Dunque, torniamo al tema principale di questo articolo.

Abbiamo detto che, quando la vendita si fa più complessa, c’è bisogno di aggiungere una ulteriore figura, quella del Technical Sales appunto.

Perché? Perché non può fare tutto il Commerciale, come nell’esempio precedente?

Semplicemente perché egli non ha le conoscenze tecniche adeguate per rispondere alle richieste dei clienti.

Il presales è una figura tecnica* che può colmare questa lacuna (vi prego di notare l’asterisco sulle parole "figura tecnica"… ci torneremo alla fine dell’articolo).

Il ruolo del Sales è di determinare la migliore strategia per approcciare la vendita. Il suo obiettivo ultimo è quello di giungere alla firma del contratto.

Il presales è più un tattico che si occupa di definire i dettagli: il Cosa, il Come, il Perché,... Il suo scopo principale è di ottenere quello che gli esperti chiamano il **Technical Win**, ovvero convincere il cliente che la soluzione che abbiamo proposto è quella più adatta a soddisfare le proprie necessità.

Siamo arrivati al punto focale della nostra spiegazione. Davvero tutto ruota intorno a questo concetto: 

> soddisfare le esigenze del cliente

D’altra parte se un cliente ci contatta, se è disposto a darci dei soldi (a volte anche molti) è perché ha dei problemi e si aspetta di ricevere da noi delle soluzioni.

## Conclusione

Abbiamo lasciato in sospeso, qualche paragrafo più su, un asterisco accanto alle parole “figura tecnica”.

È quello del presales un ruolo puramente tecnico? La risposta è NO.

Non c’è dubbio che esso debba avere delle forti competenze tecniche ma… è il mindset che deve essere diverso. Questo perché quando un “tecnico” è chiamato a parlare con un cliente, tenderà a incentrare il discorso sui prodotti: quanto sono potenti, semplici da usare e avanzati nelle funzionalità.

Questo approccio si è visto che non è efficace.

È il cliente deve essere messo al centro di tutto. Sui libri si parla non a caso della necessità di spostare l’attenzione dal **NOI** al **VOI**.

Può sembrare solo un dettaglio ma è estremamente importante perché può aumentare di molto le probabilità di una vendita.

Questo cambio di mentalità non è affatto facile da ottenere (come sempre quando bisogna mettere in discussione le proprie convinzioni) ma una volta conseguito può portare vantaggi inimmaginabili.

Ma questo è materiale per un altro post…
