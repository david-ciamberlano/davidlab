+++
title = "Presales 101.2 - il ruolo del presales"
date = 2022-02-19T10:10:00+00:00
type = "post"
draft = "false"
toc = "true"
tags = ["presales"]
categories = ["presales"]
menu = ""
+++

In un precedente post ([Presales: strategia e tattica](/post/presales/presales-101-strategia-e-tattica)) abbiamo cercato di definire la figurta del pre-sales. Ora facciamo un passo avanti e vediamo in che modo il presales può approcciare la vendita.
<!--more-->
Qui si aprono tantissimi scenari e percorsi diversi. Proverò a descrivere quelli più frequenti, basandomi sulla mia esperienza (modesta) acquisita in ormai molti anni di lavoro nel campo.

## Quali sono le attività del presales

Innanzitutto, se bisogna rispondere alle esigenze del cliente… è necessario scoprire quali sono queste esigenze.

Questo solitamente avviene in una prima fase, nella quale si organizzano meeting (virtuali o in presenza) e si cerca di ottenere quante più informazioni possibili e di scoprire i punti più “dolenti” per il cliente (che poi sono anche quelli più rilevanti per noi).

Una volta collezionate queste informazioni (e vi assicuro che spesso non è facile), bisogna utilizzarle al meglio.

Si creano uno o più scenari e si cerca di capire come i nostri prodotti possano adattarsi a essi, sempre tenendo in mente l’obiettivo principale.

Dopo aver trovato le soluzioni migliori, si può passare alla fase successiva ovvero: mostrarle al cliente. Questo può essere fatto a volte con una semplice **presentazione** o spesso con delle **demo**. È una fase molto importante in cui è bisogna sfoggiare le migliori ottime doti comunicative (l’obiettivo è convincere, ricordate?)

Questo è, in massima sintesi, uno dei possibile scenari. Ma se ne possono presentare molti altri.

Ad esempio, a volte il cliente non si accontenta di una presentazione e nemmeno di una demo ma vuole avere una prova più tangibile delle reali potenzialità dei nostri prodotti.

Chiede quindi una **POC** (Proof Of Concept) che è a tutti gli effetti un piccolo progetto o una parte del progetto finale che dobbiamo realizzare e che dobbiamo materialmente darla al cliente affinché la provi provare.

Un’altra tipica richiesta è quella degli **RFI** e/o **RFP**. RFI sta per Request For Information. In questo caso il potenziale cliente è più o meno consapevole di quali siano le proprie necessità e vuole capire in autonomia se i nostri prodotti sono adatti.

Per questo scopo, solitamente, ci viene propinata una lista interminabile di domande cui dobbiamo rispondere in modo più dettagliato possibile. A queste risposte verrà poi attribuito un punteggio. In questo modo il cliente riesce ad avere un criterio di giudizio più o meno oggettivo per capire quali prodotti possono essere più adatti alle proprie esigenze e quali meno.

    NOTA: Tipicamente quando c’è un RFI -ma anche negli altri casi- 
    non siamo gli unici attori coinvolti ma vengono chiamati anche altri competitor 
    che, ovviamente, come noi sono determinati a “vincere” la competizione.


Una scenario ancora più complesso è quello dei **bandi di gara** (tipicamente indetti da società della pubblica amministrazione). Questo è un altro grosso capitolo… ma, per farla breve, vengono prodotti documenti di decine di pagine che descrivono quello che bisogna realizzare, molto nel dettaglio fino alla stima economica del progetto.

In generale non si risponde da soli a questi bandi ma si collabora con dei partners, che sono poi quelli che dovranno realizzare il progetto in caso di vincita.

## Le attività collaterali

Quelli citate nella sezione precedente, sono solo alcuni esempi delle attività quotidiane di un presales. Sono quelle più orientate al cliente.

Ci sono però anche altre attività collaterali che il Technical Sales è chiamato a svolgere, non correlate a opportunità ma comunque importanti.

Un esempio è coltivare i rapporti con i Partners. I partners sono molto importanti perché, nella maggior parte dei casi, sono loro a realizzare i progetti dei clienti e spesso ad avere rapporti stretti con quest’ultimi.

Tra i compiti del presales ci sono: il tenere buoni contatti con loro, aggiornarli sui nuovi prodotti, organizzare dei training o semplicemente guidarli verso le scelte giuste se hanno dei dubbi.

Altre attività del Tecnical Sales sono quelle legate al Marketing: dalla pubblicazione di post sui social media, ai webinar, agli incontri o le manifestazioni. Non di rado mi è capitato di partecipare a eventi un cui avevamo stand aperti al pubblico o anche ho dovuto tenere dei talk per i potenziali clienti.

Tutte queste sono attività che molto utili per promuovere e sviluppare il business in un certo territorio.

## Conclusione

Il presales, dunque, può essere coinvolto in numerose attività (ce ne sono sicuramente altre rispetto a quanto elencato sopra) ed è forse proprio questo che rende questo ruolo difficile ma contemporaneamente appagante. Ne riparleremo... 
