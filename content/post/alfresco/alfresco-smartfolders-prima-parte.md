+++
title = "Alfresco: smart-folders (Prima Parte)"
date = 2017-11-08T23:02:16+01:00
tags = ["alfresco"]
categories = ["development"]
toc = "true"
+++
La metafora "cartelle e file" &egrave; stata introdotta nella preistoria dell’informativa (nei mitici laboratori Xerox di Palo Alto) ed &egrave; risultata tanto efficace, intuitiva e universalmente riconoscibile che ancora oggi &egrave; utilizzata praticamente da tutti i sistemi operativi...

...per&ograve;...

...quando viene applicata in ambito documentale mostra pi&ugrave; di qualche limite.
Il problema &egrave; che un albero di cartelle esprime solo uno dei tanti modi di classificare un insieme di documenti. &Egrave; per questo che chi gestisce grandi archivi si trova spesso a dover ricorrere alla funzionalit&agrave; di ricerca, per trovare i file che gli interessano.

Le smart-folders nascono per superare (o almeno mitigare) questi limiti.

<!--more-->

Quello alla base delle smart-folders non &egrave; certo un concetto nuovo. Semplificando all’estremo, possiamo dire che sono delle cartelle virtuali che contengono documenti risultanti da una ricerca.
Il fatto che siano *virtuali* &egrave; rilevante: non possono essere trattate alla stregua delle folder tradizionali(!) di Alfresco. Ad esempio, non &egrave; possibile copiare in esse dei file (ok, in effetti sotto determinate condizioni questo &egrave; possibile… ma il comportamento &egrave; diverso da quello standard).

In Alfresco le *smart-folders* possono essere di tre tipi:

- System
- Custom
- Type-based

*System* e *Custom* sono molto simili tra loro e pi&ugrave; semplici da capire. Le *Type-based* sono pi&ugrave; versatili ma un po’ pi&ugrave; complesse.

In questo post ci occuperemo dei primi due metodi. In un post successivo parleremo anche del terzo.

Cominciamo…

Per prima cosa bisogna abilitare le smart-folders in Alfresco perch&eacute; di default sono disattivate. Nel solito alfresco-global.properties settate:
```
smart-folders-enabled = true
```
Naturalmente ci sar&agrave; bisogno di riavviare l'applicazione per recepire le nuove impostazioni. Fatelo... e mentre aspettate che Alfresco torni su, potrete ingannare il tempo creando un nuovo template, che altro non &egrave; che un file json con una sintassi ben definita (nella pagina ufficiale della documentazione sulle smart-folders trovate i dettagli).

Ecco un semplice esempio:
```json
{
 "name" : "Virtual Folders Test",
 "nodes" : [{
   "id" : "101",
   "name" : "Documenti pdf",
   "description" : "I documenti pdf dell'utente contenuti nel sito",
   "search" : {
    "language" : "fts-alfresco",
    "query" : "(PATH:'/app:company_home/st:sites//*')
AND =cm:content.mimetype:application/pdf
AND (=cm:modifier:%CURRENT_USER% OR =cm:creator:%CURRENT_USER%)"
    }
   }, {
   "id" : "102",
   "name" : "Immagini",
   "description" : "Le immagini dell'utente contenute nel sito",
   "search" : {
    "language" : "fts-alfresco",
    "query" : "(PATH:'/app:company_home/st:sites//*')
AND =cm:content.mimetype:image/jpeg
AND (=cm:modifier:%CURRENT_USER% OR =cm:creator:%CURRENT_USER%)"
   }
  }]
}
```
Il significato delle sue parti &egrave; intuitivo: definiamo un oggetto che contiene una lista di nodi (due nel nostro caso). Ciascun nodo &egrave; a sua volta un oggetto con alcune propriet&agrave;: *id*, *name*, *description*, *search*.
Tra queste:

- le uniche obbligatorie sono search con le sue (sotto)propriet&agrave; language e query.
- *Id*, *name*, *description* sono opzionali. Id per&ograve; &egrave; meglio specificarlo per generare un nodeRef pi&ugrave; corto.
- Language potrebbe accettare altri valori… ma per ora &egrave; consigliabile usare solo *fts-alfresco*.

Il template appena creato va salvato in Alfresco nella cartella *"<repository>/Dizionario dati/Modelli di cartella intelligente"* (*"smart folder templates"* in inglese). Potete dargli nome -ad esempio- *smartFolder_tutorial.json*.

Ora bisogna dire ad Alfresco che quello non &egrave; un file json qualunque ma rappresenta un template di smart-folders. Per farlo dobbiamo attribuire al documento un tipo speciale. Cliccate sul nome del file per aprire la vista di dettaglio, poi sulla destra selezionate "Cambia tipo" e tra quelli proposti scegliete: "modello di cartella intelligente" (*smartFolderTemplate*).

Ci siamo quasi…
L'ultimo passo &egrave; attribuire il nostro template ad una cartella. Questo si fa associandole un aspetto ben preciso e valorizzando alcuni suoi metadati.
Dal menu delle azioni a destra selezionate "Gestisci aspetti" e poi scegliete quello di nome "Cartella intelligente di sistema". 
Poi, sempre nel menu delle azioni sulla destra, cliccate su "Modifica propriet&agrave;" per veder comparire la finestra di dialogo con il form di modifica.

Assicuratevi di mostrare tutte le propriet&agrave; (con il pulsante in alto a destra). Se avete seguito correttamente le istruzioni dovreste veder comparire in basso un ulteriore campo di nome "Modello di cartella intelligente" con una casella a discesa. Selezionate il template che avete creato in precedenza (*smartFolder_tutorial.json*) e salvate le modifiche.

Ora accadr&agrave; la magia: se aprite la cartella, scoprirete che ciascun oggetto node dichiarato nel template &egrave; stato  trasformato in una smart-folder. Il contenuto della smart-folder &egrave; il risultato della query specificata nella property query.
Il nostro esempio generer&agrave; quindi due smart-folders:

- la prima conterr&agrave; tutti i documenti di tutti i siti (PATH:’/app:company_home/st:sites//*’) che siano pdf (=cm:content.mimetype:application/pdf) creati o modificati dall’utente corrente (=cm:modifier:%CURRENT_USER% OR =cm:creator:%CURRENT_USER%).
- La seconda &egrave; analoga ma selezioner&agrave; solo i file di tipo jpg.

Una paio di cose prima di chiudere:

1. Notate che in tutte e due le query abbiamo filtrato i risultati per utente corrente. Questo non perch&eacute; per le smart-folders non valgano le regole di accesso (se non potete vedere un file, non lo vedrete!) ma per ottimizzare le performance ed evitare di recuperare documenti inutili.
2. Perch&eacute; le smart-folders funzionino correttamente, bisogna aver cura di scegliere solr4 come servizio di ricerca e di configurare la Full Text Search in modo da usare il database (“Use Database if possibile” oppure “Always use Database“).

**PS**: Vi state ancora chiedendo che differenza c'&egrave; tra i due tipi di smart-folders: System e Custom? Ecco soddisfatta la vostra curiosit&agrave;. Se usate l'aspect “Cartella intelligente di sistema” potrete scegliere solo i template presenti nella directory "<repository>/Dizionario dati/Modelli di cartella intelligente".
Se invece avete optato per "Cartella intelligente personalizzata" il template potr&agrave; essere posizionato ovunque nel repository... tutto qui.
