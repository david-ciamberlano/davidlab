+++
title = "Alfresco: Ciclo Di Vita Dei Documenti"
date = "2017-11-03T22:52:54+01:00"
featureimage = "img/sample_feature_img.png"
tags = ["alfresco", "lifecycle"]
categories = ["development"]
+++

Che fine fanno i documenti in Alfresco una volta che sono stati cancellati?
Come liberare spazio sul filesystem? Qui troverete alcune risposte a queste domande...

<!--more-->

I documento in Alfresco vengono conservati fisicamento sul filsystem nella cartella *alf_data/contentstore* che ha una alberatura che rispecchia la data+ora di inserimento del file nel repository.
```
alf_data
+--contentstore
   +--2014
      +--5
         +--7
            +--10
               +--27
                  +--<lista di file uuid.bin>
```
La cancellazione di un documento avviene in alcuni passaggi:

## Passo 1
un utente cancella il documento da interfaccia utente (share  o explorer)
Il documento in realtà non è subito eliminato dal repository ma viene spostato in una sorta di cestino (tecnicamente viene spostato nell’archive-store: archive://SpacesStore) in cui rimane finchè qualcuno non lo elimimina in modo definitivo o decide di ripristinarlo. Gli indici vengono spostati in modo analogo mentre il file fisico non viene toccato.

## Passo 2
il documento viene eliminato definitivamente dal cestino
Quando il documento viene eliminato definitivamente, il contenuto e i metadati ad esso associati vengono marcati come deleted. Il documento è diventato orfano.
PS: il cestino su share è un po’ nascosto… lo trovate nella pagina del profilo del vostro utente, nel menu in alto.

## Passo 3
store cleaner job
Periodicamente (di default una volta al giorno) viene attivato un job che si preoccupa di recuperare i nodi orfani, cancellare dal DB i loro metadati e spostare i loro file fisici nella cartella alf_data/contentstore.deleted. Anche questo passaggio non viene eseguito subito. Di default Alfresco aspetta per 14 giorni prima di eseguire il delete del file. Questa impostazione si può variare settando il valore system.content.orphanProtectDays nell’alfresco-global.properties.

## Passo 4 
svuotare la cartella *contentstore.deleted*
I file eliminati definitivamente restano in contentstore.deleted finchè non vengono cancellati manualmente. E' possibile configurare un job che si preoccupi di automatizzare questa operazione.
Se volete che i file vengano eliminati all'atto dello svuotamento del cestino, senza essere spostati nel *contentstore.deleted*, potete settare a true il parametro (che trovate sempre nell'*alfresco-global.properties*):
```
system.content.eagerOrphanCleanup=true
```


