+++
title = "Alfresco - Alcune considerazioni sulla sicurezza"
date = "2017-10-20T10:10:10+01:00"
tags = ["alfresco", "security"]
categories = ["development"]
+++

Tutti i servizi esposti su internet sono soggetti a rischi di sicurezza e Alfresco non fa certo eccezione!

In questo post riporterò alcuni consigli per la fase di installazione, utili ad impedire (magari!… meglio dire: rendere la vita un po’ più difficile) a malintenzionati di spiare o compromettere il vostro sistema. L’articolo suppone che come SO si utilizzi Linux ma i concetti sono validi in generale.

<!--more-->
**ATTENZIONE!** i consigli che riporto rappresentano solo un primo stadio di sicurezza, il minimo che serve per non avere falle banali nel sistema. Se il vostro Alfresco gestisce informazioni sensibili dovrete fare attenzione a molti altri aspetti. Nei casi più delicati sarebbe meglio non improvvisare e rivolgersi ad esperti! Nella linkografia alla fine della pagina vi riporto alcuni documenti che contengono informazioni più approfondite. Vi consiglio vivamente di leggerli.

Questo documento inoltre è in continua evoluzione. Segnalatemi pure inesattezze e aggiunte da riportare.

## Aumentare la sicurezza di Tomcat
Alfresco utilizza Tomcat quindi per prima cosa bisogna avere un’installazione sicura di tale application server. Per fortuna sembra che già di default Tomcat sia abbastanza robusto, per cui sarà necessario prendere solo pochi accorgimenti.
Regola aurea: Non avviate Tomcat con l’utente root. Create invece un utente e un gruppo ad-hoc (ad esempio alfresco:alfresco) che dovrà avere solo i minimi permessi necessari per svolgere il proprio lavoro.

File e cartelle di Tomcat dovrebbero avere i permessi minimi. Una configurazione restrittiva (ma usabile in molti casi) prevede, ad esempio, che l’owner possa fare tutto; al gruppo (nel nostro caso Alfresco) andrebbero attribuiti sono i permessi di lettura su file e cartelle mentre il resto del mondo non deve poter fare nulla (rw[x]r-[x]— per intenderci).
Se avete particolari esigenze, partendo da questa base potrete rilassare i vari criteri finché non si adattano ai vostri scopi.

**ATTENZIONE**! fate attenzione a manipolare i permessi su file e cartelle (soprattutto se non avete molta esperienza su linux). E’ infatti facile rendere inaccessibili/ineseguibili file importanti per il corretto funzionamento del sistema. Nel dubbio meglio non fare nulla.

Eliminare da tomcat tutte le altre applicazioni non necessarie (tra le quali quelle preinstallate come ad esempio ROOT, la documentazione, gli esempi…)

Fate attenzione alle applicazioni di configurazione Manager e Host Manager. Se prevedete di non usarle eliminatele.

Non attivate configurazioni di tomcat (agendo ad esempio su server.xml, web.xml, tomcat-user.xml) se non sapete bene cosa state facendo. Alfresco necessita solo di un paio di aggiunte su server.xml, che sono testate e sicure.

## Aumentare la sicurezza di Alfresco
Analogamente a tomcat, evitate di installare e avviare Alfresco da root. Potreste usare lo stesso utente che avete creato per tomcat (alfresco).

Seguite gli stessi criteri usati per Tomcat per attribuire i permessi di accesso alla cartella *alf_data* e alle altre cartelle e file di Alfresco (shared, solr4, ecc).

Cambiate il prima possibile la password dell’utente admin (e quella di JMX nella versione enterprise).

Non usate le password di default o quelle riportare nelle guide di installazione. “Inventatevi” delle keyword robuste per:

- l'utente *alfresco* con cui avete eseguito l'installazione
- la connessione al database (anche qui dovreste usare un utente ad-hoc)
- l’utente admin di alfresco
- gli altri utenti di alfresco

Controllate le password contenute nei file di properties (ad esempio plug-in ecc.)… se ci sono dovrebbero essere criptate.

Disabilitate lo user guest se non necessario (in alfresco-global.properties: *alfresco.authentication.allowGuestLogin=false*).

Disabilitate i servizi che non usate (FTP, WEBDAV, IMAP, ecc) in alfresco-global.properties:
- ftp.enabled=false
- imap.server.enabled=false
- system.webdav.servlet.enabled=false
- cifs.enabled=false
- ecc.

Fate attenzione ai permessi dei vari siti e folder di Alfresco. Se non necessario non create siti pubblici ed usate anche in questo caso il principio dei minimi permessi su qualsiasi cartella.

## Non trascurare il server
Dopo aver configurato Tomcat e Alfresco, sarebbe il caso di rendere un po’ più sicuro anche il server. Tra le prime cose da fare dovrebbe esserci il setting di un firewall. L’ideale sarebbe chiudere tutte le porte, lasciando aperte solo quelle che sono effetivamente usate. Essendo questo un argomento molto vasto che io stesso non padroneggio bene, evito di spiegarvi come procedere (la documentazione su iptables, che è installato di default nel kernel di linux, potrebbe essere un ottimo punto di partenza).

Per un server Alfresco alcune delle porte di cui potreste avere bisogno sono (nella linkografia trovate un link ad una pagina più esaustiva):

- accesso SSH (porta 22). Non è usata in modo diretto da Alfresco, ma è importante tenerla aperta soprattutto se gestite un server remoto (VPS o Cloud ad esempio)
- In generale il traffico su localhost deve essere permesso dal firewall (altrimenti alcuni servizi non potranno comunicare con il vostro server)
- Accesso http (per Alfresco, share, webdav, webscript, ecc). Esempio più comune porta=8080 (oppure porta=80 se volete usare un reverse proxy)
- Inbound email server: porta=2525  (oppure porta 25)
- Outbound email: porta necessaria per connettersi al vostro server smtp (in genere 25, se non criptata)
- Ricezione email (POP3): in genere porta=2110 (oppure 110);
- Accesso consultazione email, porta 143 (IMAP) e porta 993 (IMAP su SSL).
- FTP: in genere porta=2121 (oppure 21). Ricordate anche di lasciar aperte le porte per lo scambio dei dati… (ftp.dataPortFrom=… e ftp.dataPortTo=… nel file alfresco-global.properties)

**NOTE IMPORTANTI**: Se usate connessioni protette su SSL, le porte saranno ovviamente diverse (ad esempio per il web potrebbe essere 8443 mentre per SMTP 2465).

Se come suggerito state usando un utente ad-hoc per avviare alfresco, ricordate che su linux le porte < 1024 non sono utilizzabili dai comuni utenti.

Stiamo supponendo che avete TOMCAT+DBMS+SOLR+SHARE tutti installati sulla stessa macchina. Se invece avete dei server dedicati per alcuni di questi servizi, dovrete aprire sul firewall anche le relative porte di comunicazione

