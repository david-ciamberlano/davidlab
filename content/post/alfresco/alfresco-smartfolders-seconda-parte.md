+++
title = "Alfresco: smart-folders (Seconda Parte)"
date = "2017-11-09T23:18:32+01:00"
tags = ["alfresco"]
categories = ["development"]
toc = "true"
+++

Nel post [Alfresco smart-folders (prima parte)](/post/alfresco/alfresco-smartfolders-prima-parte), abbiamo parlato di smart-folders di tipo *System* e *Custom*... che sono molto versatili ma hanno il "difetto" di dover essere attribuite manualmente ad ogni cartella. In molti scenari questa potrebbe rivelarsi una limitazione pesante.

Le smart-folders *type-based* sono pensate per aggirare questo ostacolo: ci permettono infatti di attribuire un template ad una cartella se questa è di un certo tipo o se ha un certo aspetto.

Il procedimento da seguire è un po' più complesso dei casi precedenti... ma con un po' di pazienza non sarà difficile padroneggiarle.

<!--more-->

L’esempio che vorrei illustrarvi è una (banale) gestione di una raccolta di ebook. Per semplificare il dominio dei dati, diciamo che:

* i romanzi che prenderemo in considerazione saranno solo di tipo: Storico, Fantasy o Thriller (con buona pace degli amanti della lettura...).
* ogni libro avrà un proprietario.

Decidiamo di usare Alfresco per organizzare la nostra libreria... ma siamo troppo pigri per metterci a catalogare a mano ogni libro. Vorremmo semplicemente fare l'upload di un ebook e delegare ad Alfresco il lavoro sporco (la classificazione).

Le smart-folders type-based sono esattamente quello di cui abbiamo bisogno.
I passi che seguiremo sono questi:

* creeremo un nuovo custom model
* creeremo un template di smart-folders
* configureremo Alfresco in modo opportuno
* attribuiremo il template ad una cartella… e faremo qualche prova

## Step 1 - il model
Partiamo dal primo punto cioè la creazione del modello.

Facciamola semplice (Keep It Simple, Stupid!): useremo solo un custom type che modellerà il nostro ebook e un aspetto che ci servirà per attribuire le smart-folders ad una cartella:

* il type lo chiameremo bib:ebook ed avrà due properties:
    * bib:argomento (che potrà assumere un valore tra Storico, Fantasy e Thriller)
    * bib:proprietario (testo libero)
* l’aspect avrà nome bib:biblioSmartFolders e avrà una sola proprietà testuale:
    * bib:proprietariodefault

Vi allego il model già bello e pronto in modo da risparmiarvi la fatica(?!) di doverlo ricreare da zero (comunque non è niente di trascendentale).

## Step 2 - il template
Il secondo passo è creare un template di smart-folders. Se avete letto la prima parte del tutorial sapete già di cosa stiamo parlando: un file json con una sintassi ben definita che descrive un albero di virtual-folders e il loro contenuto. Quello che vedremo tra un paio di righe è un po’ più complicato di quello usato in precedenza.

L’albero di directory che vogliamo realizzare è questo:

* Archivio
  * Romanzi
    * Storico
    * Fantasy
    * Thriller
* Proprietari
  * Sconosciuto
  * David
  * Chiara

La directory archivio è una directory tradizionale(!) e conterrà i file degli ebook. Romanzi e Proprietari (e le rispettive sottocartelle) sono invece virtual-folders e sono costruite dinamicamente da Alfresco.

Alla fine del tutorial, quando inseriremo un nuovo ebook nella directory Archivio, lo troveremo (magicamente) anche sotto Romanzi e Proprietari, correttamente classificato nella giusta sottocartella.

Passiamo al codice. Per non allungare troppo il post, riporto solo le parti più interessanti (il template completo lo trovate, per vostra comodità, allegato all’articolo):

```JSON
"id":"2",
"name" : "Proprietari",
"description" : "Romanzi per proprietario",
"nodes" : [{
    "id":"21",
    "name":"Sconosciuto",
    "description":"Proprietario sconosciuto",
    "search" : {
        "language" : "fts-alfresco",
        "query" : "+TYPE:'bib:ebook' AND PATH:'%ACTUAL_PATH%/cm:Archivio/*' AND =bib:proprietario:'Sconosciuto'"
    }
}
```
Se avete letto la prima parte dell'articolo, non dovreste avere difficoltà nel capire lo scopo di questa porzione di json:

* generare un cartella virtuale di nome "proprietari" e una sottocartella (sempre virtuale) di nome "Sconosciuto"
* attribuire alla cartella "Sconosciuto" i documenti di tipo bib:ebook, con metadato bib:proprietario uguale a "Sconosciuto", contenuti nella cartella "%ACTUAL_PATH%/cm:Archivio".

*%ACTUAL_PATH%* è una variabile che viene sostituita da Alfresco con il path assoluto della cartella che contiene le smart-folders. In precedenza abbiamo già incontrato (e usato) un'altra variabile: *%CURRENT_USER%* che invece rappresentava l'utente corrente.

E questa era la parte facile... vediamo un altro frammento di codice:

```JSON
"nodes" : [{
    "id":"1",
    "name" : "Romanzi",
    "description" : "Romanzi",
    "nodes" : [{
        "id":"11",
        "name":"Storici",
        "description":"Romanzi storici",
        "search" : {
            "language" : "fts-alfresco",
            "query" : "+TYPE:'bib:ebook' AND PATH:'%ACTUAL_PATH% cm:Archivio/*' AND =bib:classificazione:'Storico'"
        },
        "filing" : {
            "path" : "%ACTUAL_PATH%/cm:Archivio",
            "classification" : {
                "type" : "bib:ebook",
                "aspects" : []
            },
            "properties" : {
                "bib:classificazione" : "Storico",
                "bib:proprietario" : "<bib:proprietariodefault>"
            }
        }
    }
}
```
Quella che vedete è la definizione delle virtual-folders:

* Romanzi
  * Storici
  * [...]

Leggendo la query possiamo capire qual è il contenuto della cartella *Storici*: i soli nodi di tipo *bib:ebook* che abbiano classificazione *Storico* e che siano contenuti nella cartella *%ACTUAL_PATH%/cm:Archivio*.

Rispetto al caso precedente, c'è una "novità": il blocco *filing*, di cui dobbiamo parlare più a lungo.

## Step 2.1 - filing
Il filing ci permette di eseguire l'upload di nuovi documenti in una smart-folders.

«Ma...» potreste obiettare «non ha senso! Hai detto fino ad ora che le smart-folders sono contenitori virtuali... dove vanno a finire i documenti?».

Il dubbio è più che leggittimo... e in effetti c'è un trucco.
Se esaminate il contenuto del blocco filing vedrete che è diviso in tre parti:

* path: specifica la destinazione dell'upload
* classification: indica quale tipo e quali aspetti attribuire al documento
* properties: come valorizzarne i metadati

Un po' confuso? Diciamolo con parole più semplici: se provate a fare l'upload di un documento in una smart-folder, vi ritroverete quel documento nella cartella che avete specificato in path, con il tipo/aspetto che avete indicato in classification e con le proprietà valorizzate come indicato in properties.

In pratica, nel nostro esempio, il documento finirà nella cartella Archivio, avrà il tipo ebook e saranno valorizzati i suoi due metadati classificazione e proprietario. Il primo avrà il valore fisso "Storico", il secondo erediterà il contenuto da <bib:proprietariodefault> della cartella padre (notate che il nome della property è racchiuso tra apici).

Forte no?

## Step 3 - Configurazione di Alfresco
A questo punto siamo quasi pronti. Dobbiamo solo dire ad Alfresco come e quando utilizzare il nostro template.
Per prima cosa nel file *alfresco-global.properties* bisogna creare la seguente riga:

`smart.folders.config.type.templates.qname.filter=bib:biblioSmartFolders`

Come potete vedere la property è stata valorizzata con il nome dell'aspect che abbiamo creato nel primo paragrafo.

Caricate poi il template in "*repository/dizionario dei dati/modelli cartelle intelligenti*". Attenzione! Il nome del file è importante! Deve corrispondere al valore della proprietà settata in precedenza, con un underscore al posto dei due punti. Nel nostro caso dovreste chiamarlo: bib_biblioSmartFolders.json.

Riavviate Alfresco per fargli recepire le nuove impostazioni... et voilà... siete pronti per usare le vostre smart-folders. D'ora in poi, ogni volta che una cartella avrà l'aspect bib:biblioSmartFolders, erediterà il template.

## Step 4 - In action!
Abbiamo preparato tutto... è ora di fare delle prove!
Per convenienza creiamo un nuovo sito di nome (ad esempio) test-smart-folders. Nella documentLibrary create una cartella di nome Biblioteca e in essa un'altra di nome Archivio. Nel menu delle azioni di destra della cartella Biblioteca selezionate "segue" e poi "Gestisci aspetti". Cercate l'aspect di nome "Biblioteca (bib:biblioSmartFolders)" e associatelo.
Ora, sempre dalle azioni a destra, selezionate "Modifica proprietà" poi "Tutte le proprietà". In basso vedrete comparire un nuovo campo di nome "Proprietario di default". Scrivete "Sconosciuto" nella casella di testo e salvate.

Se aprite la cartella Proprietari vedrete che al suo interno sono comparse delle smart-folders (le riconoscete dall'icona con la lente di ingrandimento):

* Proprietari
  * Sconosciuto
  * Chiara
  * David
* Romanzi
  * Storici
  * Fantasy
  * Thriller

Sono (ma guarda!) esattamente le cartelle che abbiamo definito nel template... Ma per ora sono vuote.
Provate a prendere un vostro ebook (supponiamo che sia "La bussola d'oro.pdf") e a caricarlo nella cartella "Romanzi/Fantasy". Sembrerà che non succeda nulla... ma se vi spostate nella cartella Archivio vedrete che il vostro file è lì e se controllate scoprirete che è di tipo bib:ebook e ha i metadati classificazione e proprietario valorizzati rispettivamente con "Fantasy" e "Sconosciuto".
Tornate ora nella cartella "Romanzi/Fantasy"... lo stesso ebook è presente anche lì (se non lo trovate aspettate qualche secondo per dare la possibilità a Solr di indicizzarlo).
Controllate anche la cartella "Proprietari/Sconosciuto"... dovreste trovare di nuovo lo stesso file!
Ora provate a modificare il metadato Proprietario settandolo a "David". Dopo qualche secondo il file dovrebbe sparire dalla cartella "Proprietari/Sconosciuto" e spostarsi in "Proprietari/David".

## Conclusioni
Le smart-folders sicuramente hanno grandi potenzialità... Ma in Alfresco sono state incluse da pochissimo, ed è ancora un po' presto per dire quanto e come saranno recepite dagli utenti. Ci saranno problemi di performance? Si riveleranno alla fine poco versatili (almeno nella loro forma attuale)?

Ci tocca aspettare per saperlo (ma intanto iniziate ad utilizzarle!).


---
Link ai json usati nel tutorial

[bibliosmartfolders.json.zip](/files/bib_biblioSmartFolders.json.zip)

[bib_biblioSmartFolders.json.zip](/files/bib_biblioSmartFolders.json.zip)
