+++
title = "Alfresco Notarization With Algorand Blockchain"
date= 2020-10-19T15:53:38+02:00
draft = false
toc = true
tags = ["alfresco", "blockchain"]
categories = ["blockchain"]
featureimage = ""
menu = ""
+++

## What the Alfresco Notarization extension does
Notarization is a lot of things… but this is not the right place to talk about this complex topic. This Alfresco extension focuses on just two of its aspects:

- proof of existence
- proof of ownership

<!--more-->
	
In an ECM environment (until now) the most used method to achieve these two objectives is to use a digital signature and a legal timestamp service. These services are provided by an external authority and are quite expensive and not easy to implement and integrate.
As we will see later, blockchain is a very good candidate to replace (or better: completely remove) the external company.
The advantages of adopting blockchain technology for the Notarization are various:

1. You don’t need an external certification authority since blockchain is a decentralized  system
2. You can obtain (and verify) proof of existence and proof of immutability in a single atomic transaction
3. Blockchain is secure by design and reachable from all over the world
4. Blockchain can store additional information on the document.
5. You have to pay the transaction only once, for an unlimited time, and the cost is only a fraction of the equivalent cost of the digital signature and legal timestamp.
	
The Notarization extension adds two new actions in Alfresco:

1. Notarize
2. Verify Notarization
	
The first one permits the user to notarize a document stored in Alfresco  (i.e. register its hash in the blockchain). The second one allows the user to verify if the notarization of a document is still valid.

We will examine the two actions in detail later.

## What’s Blockchain and why Algorand
Blockchain is a continuously growing chain of blocks. Each block can contain zero or more transactions.

The most popular and used blockchains are, at the moment, Bitcoin and Ethereum (and their derivatives). Both of them could be used for the Notarization however they have some limitations related to time and costs. For example, because of the occasional forks in the blockchain, the confirmation of a transaction could take minutes (or even hours). Another problem is that the fee required for each transaction could be high.

Algorand is a relatively new blockchain, backed by an advanced technology developed by a team of researchers, mathematicians, cryptographers, and economists led by Silvio Micali (professor at Boston MIT and winner of  the Touring Award and the Godel Prize).

It’s based on a Proof-of-Stake consensus paradigm called "Pure proof of stake" and it is:

- Decentralized (no central authority or single point of control)
- Secure
- Accurate
- Transparent 
- Open-source
- Low cost
	
In the Algorand blockchain, a new block (and consequently its transactions) is created and definitively confirmed about every five seconds and it costs only a fraction of a cent.
A transaction is signed with the private key of the "sender" and contains quite a lot of information. The most useful (in our case) are:

- Unique transaction ID
- Timestamp
- Sender and Receiver accounts (Wallets)
- Note Field (up to 1kb)
- Fee
	
Once finalized, a transaction cannot be modified or deleted.

### Proof of existence
This serves to prove that a document existed at a certain time and has not been modified afterwards. We can do that in two steps:

1. compute the hash of the document 
2. send (store) this hash in a blockchain transaction
	
Each transaction can contain up to 1kb of data (bytes) in its note field. The Notarization extension uses this field to store a json object that contains the previously computed hash and other information on the document.
 
Once the hash is sealed in a transaction, we can guarantee that the document has not changed simply comparing its hash with the one in the blockchain. If only one bit is altered, the document hash will be completely different.

This is exactly the same procedure used by the digital signature process.

### Proof of ownership
One of the fields registered in the transaction is the sender’s account address.
Since the sender is the only one who has the private key of that address, he is the only one who can certify that he has made that specific transaction at that specific time toward that specific wallet.

## Notarization extension implementation
As we said before, this add-on extends Alfresco’s capabilities with two actions: 

1. Notarize
2. Verify Notarization: 

Because they are Actions, both of them can be used directly on a document using Share (or ADF) or can be invoked by a rule, a batch process or Alfresco Behaviour. 

Any document stored in Alfresco can be notarized and any type of digital file is supported (MSOffice docs, texts, images, videos, pdf, xml, …). 

### Notarize action
This action permits the user to notarize a document stored in Alfresco.

![Image 1](/img/algo-notarization/image1.png)

It computes its hash (sha256), builds a json object with the hash and other related metadata and finally creates a transaction in the Algorand blockchain. The json object is stored in the note field of the transaction. A new aspect, containing metadata related to that transaction (in particular: hash, block ID, transaction ID, transaction time, account address), is associated with the document in Alfresco.

![Image 2](/img/algo-notarization/image2.png)

The "Verification URL" field contains the url to a website that permits the user to explore the Algorand blockchain, its blocks and its transactions.
In this case, the url brings you to a website that shows the transaction created by the notarization action.

![Image 3](/img/algo-notarization/image3.png)

### Verify Notarization
This action permits the user to verify if a previous notarization is still valid. It computes the document hash, retrieves the transaction from the Algorand blockchain and matches the computed hash and the timestamp with the ones stored in the transaction. If they match, the document is still valid.

If the date or just a single bit in the file changes (and thus the hash is completely different) the verification fails.

## Algorand Node
This demo uses an independent node switched on the Algorand test-net. Transactions on test-net are free of charge. You can find more information on how to install a node here: [https://developer.algorand.org/docs/run-a-node/setup/install/](https://developer.algorand.org/docs/run-a-node/setup/install/)

**NOTE**: this demo uses a non-archival node. This implies that only the last 1000 blocks are maintained and searchable. Since a new block is created every ~5 seconds, you can search for nodes not older than 1 hour and 25 minutes.

## References
- Github: https://github.com/david-ciamberlano/aa-notarization
- Documentation & demo: https://github.com/david-ciamberlano/aa-notarization/tree/master/Documentation
