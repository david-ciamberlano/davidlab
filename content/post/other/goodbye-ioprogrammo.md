+++
title = "Goodbye ioProgrammo"
date = "2018-06-01T10:00:00+01:00"
tags = ["other"]
categories = ["my activities"]
+++

Pensavo di iniziare questo post con qualcosa di serioso, un epitaffio o più semplicemente qualche frase ad effetto (ovviamente ~~scopiazzata~~ intelligentemente elaborata da un pensiero di qualcun altro).
Ma che si può dire sulla chiusura di una delle più longeve e lette riviste (nonché l’unica, negli ultimi tempi) di programmazione in Italia?

Ripiego quindi sui ricordi, che poi sono l’unica cosa che mi rimane, insieme ai numerosi “numeri” della rivista che ancora più o meno gelosamente conservo (almeno quelli su cui sono comparsi i miei articoli).

{{< figure src="/img/goodbye-ioprogrammo/ioprogrammo-numeri-david.jpg" caption="Tutti i numeri di ioProgrammo con almeno un mio articolo" >}}
<!--more-->

## I Ricordi Migliori
Il primo ricordo che ho è legato al primo articolo pubblicato. 
Avevo già qualche esperienza: avevo scritto sporadicamente per Linux Pro, Linux&Co (Gloriosa Linux&Co) e per una rivista giuridica italiana (il Nuovo Diritto), che aveva una rubrica divulgativa su temi informatici. 
Fui però ugualmente entusiasta quando l’allora redattore (Piero Mannelli, nel lontano 2012) approvò la mia proposta e il mese successivo vidi pubblicato il mio pezzo sulla rivista: un articolo su come rendere multilingue i software in C#/.Net (framework che avrei poi abbandonato e mai più utilizzato in futuro).

Anche se a quei tempi non lo sapevo, ne sarebbero seguiti molti altri (più di 30 negli anni successivi)… e dal 2014, con l’ottimo Raffaele del Monaco che era diventato nel frattempo caporedattore, la mia collaborazione è divenuta costante con 1 o più articoli al mese (sono arrivato fino a 3!).

A questo “ritmo” di pubblicazione è legato un altro dei ricordi legato alla rivista: la difficoltà nel trovare argomenti nuovi.

Perché scrivere un articolo ogni tanto può non essere così difficile… ma inventarsi uno (o più) progetti diversi al mese per molti mesi di fila diventa un’attività per niente banale!

Innanzitutto i temi da trattare finiscono! I primi 4 o 5 pezzi si ispirano immancabilmente a esperienze passate (di lavoro o per hobby)… poi però bisogna usare la fantasia e, soprattutto, la propria cultura. 

E non pensate di cavarvela con i “semplici” tutorial su nuove tecnologie! La rivista ha un suo pubblico che ha determinate aspettative. Un articolo su come salvare una Entity con Hibernate o come creare un component in Angular non interessa a nessuno (online si trovano tonnellate di blog che parlano di queste cose). 
Bisognava proporre progetti reali (o per lo meno realistici) e comunque dal taglio assolutamente pratico. 
E vi assicuro che non è affatto semplice scovare argomenti di interesse per il grande pubblico, sempre diversi, ogni mese!

Ulteriore difficoltà (e ulteriore ricordo!): le battaglie con i vari redattori (battaglie sempre molto fair, ovviamente! Si discuteva più che altro) perché tendevo sempre a infilare troppa teoria nei articoli… tanto che, già da quando proponevo la scaletta del pezzo (prima di scriverlo) la frase ricorrente che ricevevo come risposta era: “mi raccomando: devi dare un taglio pratico! Fai esempi o usa un caso d’uso concreto!”. Raccomandazioni quasi sempre disattese nella stesura finale.

Mi rendevo conto benissimo che questa ostinazione verso la praticità era dovuta al target della rivista e a quello che i lettori cercavano… ma uno non può cambiare la propria natura :)

Questi vincoli si sono però rivelati molto utili nella mia carriera lavorativa: mi hanno insegnato che bisogna sempre focalizzarsi sui scenari reali. 
Un progetto (di qualsiasi natura esso sia) per funzionare deve essere prima di tutto utile a qualcuno!

## I reportage
Un altro bel ricordo è stato il mio “battesimo” come reporter.  
Tanto per riagganciarmi al paragrafo precedente, tutto ebbe inizio da un bisogno reale: volevo entrare gratis al Codemotion… (sì ok, non è una motivazione nobile… ma è la realtà). 
Escludendo eventuali amicizie tra gli organizzatori -che non avevo-, gli unici due modi che conoscevo per raggiungere lo scopo erano: diventare speaker o avere l’accredito stampa.
Avevo provato mesi addietro con una submission per un talk ma non era stata accettata. Mi restava la seconda carta da giocare. Ho proposto la cosa all’allora redattore (Raffaele del Monaco) che mi ha assecondato con entusiasmo. 

C’era però un ma (c’è sempre un ma!): per essere efficace l’articolo doveva uscire nel successivo numero della rivista, che sarebbe stato chiuso il giorno successivo alla conference.  Avevo insomma solo un giorno per consegnare 3 pagine + foto.

Ora, se tre pagine di rivista sembrano poche sappiate che non lo sono affatto: bisogna raccogliere il materiale, catalogarlo, selezionarlo, trovare la giusta chiave narrativa e infine… scrivere! 
Ma la sfida era accettata: la domenica notte alle 2 il pezzo era finito, una settimana più tardi lo si poteva leggere nelle edicole. Ed io mi sentivo tanto un “reporter”.

Di reportage ne sono seguiti altri: il Codemotion dell’anno successivo, e vari Google I/O e Microsoft Build (seguiti da remoto, ovviamente).

## Gli articoli
Di articoli, nell’arco di 4 anni, ne ho scritti più di una trentina. Spesso è stato faticoso ma mi ha “costretto” a studiare nuove tecnologie e a mettere in funzione il cervello per trovare nuovi argomenti.

Guardando indietro, sono contento del materiale che riuscito a tirar fuori. 
Ho scritto articoli sui svariati argomenti:

* Strumenti enterprise come: Docker, Alfresco, Storm, MongoDB.
* Vari framework e librerie Javascript (da Leaflet a Impressjs, passando per Angular, Polymer)
* Framework Java: Spring/mvc, Hibernate, JSF
* Argomenti puramente teorici: Design patterns, MVC pattern, Regular expressions, Linguaggio Scala
* Reportage: Codemotion Roma, Google I/O, Microsoft Build, Java 8 e 9
* Argomenti “meno convenzionali”: Alfresco, Cookie Law, Bash Shell, I migliori linguaggi di programmazione, GIS

Insomma un ottima attività collaterale e sicuramente uno stimolo continuo per la mente.

## Cronaca di una morte (non) annunciata
La fine era nell’aria… non è stata annunciata, né eclatante. Semplicemente la rivista si è spenta senza grossi clamori ma lasciando un grosso vuoto.
Io ricordo solo che stavo scrivendo un nuovo articolo quando ho ricevuto una email dal redattore che mi diceva di non continuare perché (cito testualmente) “cerano stati dei cambiamenti nella rivista”. Ho intuito subito che il “cambiamento” era drastico…

Di segnali nell’aria ce n’erano, evidenti, da mesi: la rivista era diventata prima a tiratura bimestrale poi di nuovo mensile ma con un numero di pagine ridotto…

Ma il segnale più forte per me era stato che... i pagamenti per i miei articoli erano stati via via diluiti fino a cessare definitivamente. Ho ancora almeno una decina di articoli non retribuiti (e i soldi difficilmente li vedrò a quanto pare...). 
Ho continuato a scrivere nonostante tutto forse perché per me era una passione (e il compenso solo un evento collaterale!). O forse perché volevo dentro di me che quell’avventura non finisse mai…

Goodbye IoProgrammo…












