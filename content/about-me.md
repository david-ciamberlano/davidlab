---
title: "David Ciamberlano"
date: 2017-12-01T00:00:01+02:00
---

<div style="text-align: center; font-size:48px; margin-bottom: 1em;">About me...</div>

## Ambito professionale
Lavoro in [ForgeRock](https://www.forgerock.com/) e mi occupo attualmente di IAM (Identity and Access Management). Sono **Senior Solutions Engineer** per l’Italia:
se avete bisogno di informazioni tecniche, Demo, POC, consigli su come impiegare al meglio i prodotti ForgeRock, allora sono la persona giusta per voi!

In precedenza ho svolto per molti anni consulenza e attività di pre-sales su Alfresco e più in generale su Enterprise Content e Process management.

Scavando ancora di più nel mio passato:

- Ho ricoperto ruoli di team leader e solution architect in aziende medio-grandi, gestendo prevalentemente progetti java-based ma non disdegnando mai di “sporcarmi le mani” con la parte front-end e, negli ultimi anni, con containers e orchestratori (docker + Kubernetes).
- Sono stato per vari anni free-lance, come esperto **GIS** (**G**eographic **I**nformation **S**ystem) per Autodesk ma anche come sviluppatore java, C# e .net, Javascript, PHP per alcune grandi aziende Italiane (da free-lance, si sa, si cerca di fare un po’ di tutto… salvo poi accorgersi che disperdere le proprie limitate energie su un fronte così ampio alla lunga non paga)
- Agli albori della mia carriera ho lavorato in ambito SAP, in particolare sulla **Business Intelligence** (SAP BI).

Last but not least: ho la maturità classica, mi sono laureato in Ingegneria Elettronica e… non ho smesso mai di studiare cose nuove (l’unica vera costante nella mia carriera lavorativa).

## Comunicazione e Insegnamento
Insegnare e comunicare concetti tecnici è una mia vocazione. La assecondo dilettandomi nella “professione” (le virgolette le metto perché per me in realtà è una passione) di **technical writer**, erogando **formazione** tecnica a vari livelli in fondazioni ed enti privati, organizzando meetup e webinar e scrivendo blog-post divulgativi ogni volta che posso.

Nel corso degli ultimi anni ho pubblicato più di **40 articoli** tecnici per numerose riviste nazionali, tra le quali IoProgrammo, Linux&Co, LinuxPro, In Nuovo Diritto, …

Ho coordinato l’**Alfresco hackathon** di Roma a settembre 2017 e ho organizzato il primo **Alfresco Meetup** italiano a Roma nel giugno 2018.

Ho inoltre partecipato come speaker all’**Alfresco DevCon** di Lisbona a gennaio 2018 (dove ho ricevuto anche un **premio** per il mio attivismo nella community) con due lightning talks e al Red Hat Open Source day 2019 di Roma e Milano.

## Tempo libero
Nel (poco) tempo libero, sono un family-man e un amante della lettura, sia narrativa che tecnica.
In passato ho avuto numerose esperienze di volontariato con ragazzi disagiati e anziani, ho fatto parte per anni di una compagnia teatrale amatoriale e praticato fotografia.

In questa pagina alcuni post che mi riguardano: [David Ciamberlano](../categories/david-ciamberlano/)


