let lunrIndex, allPosts;



fetch("/json/search.json")
    .then(response => {
        return response.json();
    })
    .then(response => {
        allPosts = response;
        lunrIndex = lunr( function() {

            this.ref('uri');

            this.field("title", {
                boost: 10
            });

            this.field("tags", {
                boost: 5
            });

            this.field("content", {
                boost: 3
            });

            response.forEach( item => {
                this.add(item);
            }, this);
        });
    })
    .catch(error => {
        console.error(error);
    });

console.log()
// perform search
document.getElementById("search-button").onclick = search;
// document.getElementById("search-field")
//     .addEventListener("keydown", function(event) {
//         if (event.key === "Enter") {
//             event.preventDefault();
//             search();
//         }
//     });



function search() {

	const query = document.getElementById("search-field").value;

	console.log(query);
    const searchResults = lunrIndex.search(query);
    console.log(searchResults);

    // filter by score is for future use
    const uriResults = searchResults.filter((item)=> item.score > 0).map( currentElem => currentElem.ref);
    const results = allPosts.filter( currentElem =>  uriResults.includes(currentElem.uri));
    // create DOM elements
    document.createElement("h1");
	const resultsEl = document.getElementById("results");
	const foundEl = document.getElementById("found");
	resultsEl.innerText = "";
	foundEl.innerText = "";

    if (results.length === 0) {
		foundEl.innerText = 'No results Found';
		let header = document.createElement("h2");
		header.innerText = "No document found";
		resultsEl.appendChild(header);
	}
	else {
		results.forEach((v) => {
			let anchor = document.createElement("a");
			anchor.setAttribute("href", '/post' + v.uri);
			anchor.innerHTML = "<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n" + v.title;

			let header = document.createElement("h2");
			header.appendChild(anchor);

			resultsEl.appendChild(header);
			foundEl.innerText = `Found ${results.length} results`;

		});
	}

}


